package test.model.estructuras;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Before;

import model.vo.LocationVo;
import model.estructuras.PriorityQueue;
import model.estructuras.Heap;
import model.estructuras.ListaDobementeEncadenada;

import org.junit.Test;

public class PruebaColaPrioridad {
	private Heap<LocationVo> heap; 
	private PriorityQueue<LocationVo> pq; 
	private ArrayList <LocationVo> arreglo; 
	@Before
	public void setUpEscenario1()
	{
		heap = new Heap<LocationVo> ();
		pq = new PriorityQueue<>();
		arreglo = new ArrayList<>();
		arreglo.add(new LocationVo("LAALALALAP", 8, 1));
		arreglo.add(new LocationVo("AmigaDateCuenta", 8, 2));
		arreglo.add(new LocationVo("Per�", 9, 5));
		arreglo.add(new LocationVo("Come", 9, 5));
		arreglo.add(new LocationVo("lilo", 8, 4));
		arreglo.add(new LocationVo("ioNoS�", 8, 3));
		arreglo.add(new LocationVo("QuePasara", 8, 2));
		arreglo.add(new LocationVo("LAALALALALLALALALA", 8, 1));
	}
	
	

	@Test
	public void testHeap() {
		for (LocationVo a : arreglo)
		{
			try
			{
				heap.insert(a);
			} catch (Exception e) {
				fail ("No deber�a mandar excepci�n");
			}
			
		}
		
		Collections.sort(arreglo);
		
		for (int i=arreglo.size()-1;i>=0;i--)
		{
			assertEquals(arreglo.get(i), heap.delMax());
		}
		
	}
	@Test
	public void testQueue() {
		for (LocationVo a : arreglo)
		{
			try
			{
				pq.add(a);
			} catch (Exception e) {
				fail ("No deber�a mandar excepci�n");
			}
			
		}
		
		Collections.sort(arreglo);
		
		for (int i=arreglo.size()-1;i>=0;i--)
		{
			assertEquals(arreglo.get(i), pq.delMax());
		}
	}

}
