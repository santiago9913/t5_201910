package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;

import javax.print.attribute.standard.NumberOfDocuments;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.bean.OpencsvUtils;
import com.sun.xml.internal.org.jvnet.fastinfoset.VocabularyApplicationData;

import model.estructuras.Heap;
import model.estructuras.PriorityQueue;
import model.estructuras.Stack;
import model.vo.LocationVo;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;
import model.util.Quick;

public class Controller
{

	private static final String[] FECHAS = {
			"./data/January_2018.csv",
			"./data/February_2018.csv",
			"./data/March_2018.csv",
			"./data/April_2018.csv"

	}; 

	private Heap<VOMovingViolations> datosHeap; 
	private PriorityQueue<VOMovingViolations> datosQueue; 
	private MovingViolationsManagerView view; 
	private VOMovingViolations [] muestra; 
	private VOMovingViolations [] datos; 



	/**
	 * Inicializar los arreglos
	 */
	public Controller()
	{
		datosHeap = new Heap<VOMovingViolations>(0); 
		datosQueue = new PriorityQueue<VOMovingViolations>(0);
		view = new MovingViolationsManagerView(); 
		muestra = new VOMovingViolations [0];
		datos = new VOMovingViolations[0];

	}
	/**
	 * Genera una muestra de n elementos y la guarda en el arreglo muestra
	 * @param n un entero del tama�o de la muestra
	 */
	public  void generarMuestra ( int n)
	{
		muestra = new VOMovingViolations [n];
		int pos = (int) (Math.random()*(datos.length-1));

		for (int i=0; i< n;i++)
		{
			if (pos== datos.length-1)
			{
				pos=0;
			}
			VOMovingViolations actual = datos[pos];
			pos++;
			muestra[i]=actual;
		}

	}
	/**
	 * Carga los datos en un Heap y Priority Queue
	 * @throws IOException 
	 */
	public void loadMovingViolations()
	{
		try {
			loadFiles(FECHAS);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * M�todo para realizar una gr�fica mostrando los tiempos promedios de ejecuci�n para los m�todos agregar y
	 * delMax usando las dos estructuras con diferentes cargas; es decir con diferentes vol�menes de
	 * datos [0, 350000] con un delta de 50000. 
	 */
	public void noLoVoyAHacerAMano ()
	{
		for (int i=50000;i<=350000;i+=50000)
		{
			System.out.println("GENERAR "+i);
			generarMuestra(i);

			long startTime3 = System.currentTimeMillis();
			cargarMuestraHeap();
			long endTime3 = System.currentTimeMillis() - startTime3;
			System.out.println("Cargando en el heap "+muestra.length+" datos, se demoro "+endTime3);
			startTime3 = System.currentTimeMillis();
			cargarMuestraPriorityQueue();
			endTime3 = System.currentTimeMillis() - startTime3;
			System.out.println("Cargando en el priorityQueue "+muestra.length+" datos, se demoro "+endTime3);
			startTime3 = System.currentTimeMillis();
			eliminarTodoHeap();
			endTime3 = System.currentTimeMillis() - startTime3;
			System.out.println("Eliminando en el heap "+muestra.length+" datos, se demoro "+endTime3);
			startTime3 = System.currentTimeMillis();
			eliminarTodoPriorityQueue();
			endTime3 = System.currentTimeMillis() - startTime3;
			System.out.println("Eliminando en el priorityQueue "+muestra.length+" datos, se demoro "+endTime3);

		}
	}

	public void cargarMuestraPriorityQueue ()
	{
		datosQueue = new PriorityQueue<>(muestra.length);

		for (VOMovingViolations a: muestra)
		{
			datosQueue.add(a);
		}
	}

	public void cargarMuestraHeap ()
	{
		datosHeap = new Heap<>(muestra.length);
		for (VOMovingViolations a: muestra)
		{
			datosHeap.insert(a);
		}
	}
	public void eliminarTodoHeap ()
	{
		int tam= datosHeap.size();
		while (tam>0)
		{
			datosHeap.delMax();
			tam--;
		}
	}
	public void eliminarTodoPriorityQueue ()
	{
		int tam= datosQueue.size();
		while (tam>0)
		{
			datosQueue.delMax();
			tam--;
		}
	}
	public PriorityQueue <LocationVo> crearMaxColaP (LocalDateTime fInicial, LocalDateTime fFinal)
	{
		PriorityQueue <LocationVo> res = new PriorityQueue <LocationVo>();
		String location = null;
		int  addressId=0, numberOfRegisters=0;
		for ( VOMovingViolations a : datos)
		{
			LocalDateTime local = a.getTicketIssugeDateLocalTime();

			if (local.compareTo(fInicial)>=0 && local.compareTo(fFinal)>=0)
			{

				if (location == null)
				{
					location = a.getLocation();
					try
					{
						addressId = Integer.parseInt(a.getAddressId());
					}catch (Exception e)
					{
						addressId = 0;
					}
					numberOfRegisters = 1;
					continue;
				}
				else 
				{
					int Id =0;
					try
					{
						Id = Integer.parseInt(a.getAddressId());
					}catch (Exception e)
					{
						Id = 0;
					}
					if ( location.equals(a.getLocation()) && addressId == Id )
					{
						numberOfRegisters ++;
					}else
					{
						res.add(new LocationVo(location, addressId, numberOfRegisters));
						location = a.getLocation();

						addressId = Id;

						numberOfRegisters = 1;
					}
				}
			}
		}
		if (location != null)
		{
			res.add(new LocationVo(location, addressId, numberOfRegisters));
		}
		return res ;
	}
	public Heap <LocationVo> crearMaxHeapCP (LocalDateTime fInicial, LocalDateTime fFinal)
	{
		Heap <LocationVo> res = new Heap <LocationVo>();
		String location = null;
		int  addressId=0, numberOfRegisters=0;
		for ( VOMovingViolations a : datos)
		{
			LocalDateTime local = a.getTicketIssugeDateLocalTime();
			if (local.compareTo(fInicial)>=0 && local.compareTo(fFinal)<=0)
			{

				if (location == null)
				{
					location = a.getLocation();
					try
					{
						addressId = Integer.parseInt(a.getAddressId());
					}catch (Exception e)
					{
						addressId = 0;
					}

					numberOfRegisters = 1;
					continue;
				}
				else
				{
					int Id =0;
					try
					{
						Id = Integer.parseInt(a.getAddressId());
					}catch (Exception e)
					{
						Id = 0;
					}
					if ( location.equals(a.getLocation()) && addressId == Id )
					{
						numberOfRegisters ++;
					}else
					{
						res.insert(new LocationVo(location, addressId, numberOfRegisters));
						location = a.getLocation();
						addressId = Id;
						numberOfRegisters = 1;
					}
				}
			}
		}
		if (location != null)
		{
			res.insert(new LocationVo(location, addressId, numberOfRegisters));
		}
		return res;
	}
	/**
	 * Carga los CSV
	 * @param meses
	 * @param dato
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	private void loadFiles(String[] meses) throws NumberFormatException, IOException
	{
		String row[];
		int cou = 0;
		Stack<VOMovingViolations> stack = new Stack<VOMovingViolations> (); 
		for(int i = 0; i < meses.length; i++)
		{
			FileReader fileReaderMes = new FileReader(FECHAS[i]);
			CSVReader mes = new CSVReaderBuilder(fileReaderMes).withSkipLines(1).build();

			while((row = mes.readNext()) != null)
			{
				int objectId = Integer.parseInt(row[0]); 
				int totalPaid = (int)Double.parseDouble(row[9]);
				short fi = Short.parseShort(row[8]);
				short penalty1 = Short.parseShort(row[10]);
				stack.push(new VOMovingViolations(objectId, totalPaid,  fi,  row[2], row[13],
						row[12],row[14], row[15], row[4], row[3], penalty1));

			}

		}

		datos = new VOMovingViolations[stack.size()];
		Iterator<VOMovingViolations> it = stack.iterator();
		int i=0;
		while(it.hasNext())
		{
			VOMovingViolations actual = it.next();
			actual.changeComparator(7);
			datos[i] = actual; 
			i++; 
		}
		Quick.sort(datos);
	}


	public void run() 
	{
		Scanner sc = new Scanner(System.in); 
		boolean fin = false; 

		while(!fin)
		{
			System.out.println();
			view.printMenu();

			int opcion = sc.nextInt();

			switch (opcion) 
			{
			case 1:
				loadMovingViolations();
				view.printMenssage("Datos cargados: "+ datos.length);
				break;
			case 2:
				view.printMenssage("Ingrese N");
				int n1=sc.nextInt();

				view.printMenssage("Ingrese la fecha con hora inicial (Ej : 28/03/2017T15:30:20)");
				LocalDateTime fechaInicialReq2A = convertirFecha_Hora_LDT(sc.next());

				view.printMenssage("Ingrese la fecha con hora final (Ej : 28/03/2017T15:30:20)");
				LocalDateTime fechaFinalReq2A = convertirFecha_Hora_LDT(sc.next());

				long startTime8 = System.currentTimeMillis();
				PriorityQueue<LocationVo> pq = crearMaxColaP(fechaInicialReq2A, fechaFinalReq2A);
				long endTime8 = System.currentTimeMillis() - startTime8;
				view.printMenssage("El priority queue se demoro "+endTime8+" milisegundos");

				startTime8 = System.currentTimeMillis();
				Heap<LocationVo> heap =  crearMaxHeapCP(fechaInicialReq2A, fechaFinalReq2A);
				endTime8 = System.currentTimeMillis() - startTime8;
				view.printMenssage("El heap se demoro "+endTime8+" milisegundos");

				view.printLocation(n1, pq, heap);
				break;
			case 3:
				view.printMenssage("Ingrese el n�mero de la muestra:");
				int n = sc.nextInt();
				if (n>datosHeap.size())
					view.printMenssage("No hay ese n�mero de datos");
				else
					generarMuestra(n);
				break;
			case 4:
				long startTime = System.currentTimeMillis();
				cargarMuestraPriorityQueue();
				long endTime = System.currentTimeMillis() - startTime;
				System.out.println("Cargando "+muestra.length+" datos, se demoro "+endTime);
				break;
			case 5:
				long startTime2 = System.currentTimeMillis();
				cargarMuestraHeap();
				long endTime2 = System.currentTimeMillis() - startTime2;
				System.out.println("Cargando "+muestra.length+" datos, se demoro "+endTime2);
			case 6:
				long startTime3 = System.currentTimeMillis();
				eliminarTodoHeap();
				long endTime3 = System.currentTimeMillis() - startTime3;
				System.out.println("Cargando "+muestra.length+" datos, se demoro "+endTime3);
				break;
			case 7:
				long startTime4 = System.currentTimeMillis();
				eliminarTodoPriorityQueue();
				long endTime4 = System.currentTimeMillis() - startTime4;
				System.out.println("Cargando "+muestra.length+" datos, se demoro "+endTime4);
				break;
			case 8:
				sc.close();
				break;
			case 69:
				noLoVoyAHacerAMano();
				break;

			}
		}
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		//		"dd/MM/yyyy'T'HH:mm:ss"
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("dd/MM/yyyy'T'HH:mm:ss"));
	}

}
