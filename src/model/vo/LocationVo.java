package model.vo;

public class LocationVo implements Comparable<LocationVo>
{
	private int addressId, numberOfRegisters;
	private String location; 
	
	public LocationVo(String location, int addressId, int numberOfRegisters)
	{
		this.location = location;
		this.addressId = addressId;
		this.numberOfRegisters = numberOfRegisters; 
	}

	@Override
	public int compareTo(LocationVo o) 
	{
		int comparacion = Integer.compare(this.numberOfRegisters, o.numberOfRegisters); 
		
		if(comparacion == 0)
		{
			comparacion = this.location.compareTo(o.location); 
		}
		
		return comparacion;
	}
	@Override
	public String toString()
	{
		return "Locaci�n: "+ location +" AddresID: "+addressId + " Numero de registros: "+numberOfRegisters;
	}
}
