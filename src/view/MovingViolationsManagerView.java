package view;

import model.estructuras.Heap;
import model.estructuras.PriorityQueue;
import model.vo.LocationVo;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() {
		
	}
	
	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 5----------------------");
		System.out.println("1. Cargar datos de infracciones en movimiento");
		System.out.println("2. Consultar las N v�as que tienen la mayor cantidad de infracciones registradas en un periodo de tiempo");
		System.out.println("3. Generar muestra");
		System.out.println("4. Cargar muestra a PriorityQueue");
		System.out.println("5. Cargar muestra a Heap");
		System.out.println("6. DelMax a todo el PriorityQueue");
		System.out.println("7. DelMax a todo el Heap");
		
		System.out.println("8. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	public void printDatosMuestra( int nMuestra, Comparable [ ] muestra)
	{
		for ( Comparable dato : muestra)
		{	System.out.println(  dato.toString() );    }
	}
	public void printLocation( int n, PriorityQueue<LocationVo> pq, Heap<LocationVo> hp)
	{
		n= Math.min(n,hp.size());
		n--;
		while (n>=0  )
		{
			System.out.println(hp.delMax());
			n--;
		}
	}
	
	public void printMenssage(String mensaje) {
		System.out.println(mensaje);
	}
}